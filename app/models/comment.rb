class Comment < ApplicationRecord
  belongs_to :article
  before_save { self.email = email.downcase}
  include Visible
  validates :status, inclusion: { in: VALID_STATUSES }
  validates :email, email: true

  # def archived?
  #   status == 'archived'
  # end

end
