class RegistrationJob < ApplicationJob
  queue_as :default

  def perform(comment)
    # Do something later
    EmailSenderMailer.comment_email_notify(comment).deliver_later(wait: 30.second)
  end
end
