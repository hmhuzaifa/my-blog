class EmailSenderMailer < ApplicationMailer
    def comment_email_notify(comment)
        @comment=comment
        mail(to: @comment.email, subject: "You Have an comment on an article!")
    end
end 
