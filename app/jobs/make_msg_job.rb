class MakeMsgJob < ApplicationJob
  require 'twilio-ruby'
  require 'rubygems'
  queue_as :default


  def perform(article)
    @article = article
    client = Twilio::REST::Client.new ENV["TWILIO_ACCOUNT_SID"], ENV["TWILIO_AUTH_TOKEN"]
    message = client.messages.create(
      :from => ENV["TWILIO_NUMBER"] ,
      :to => '+923004276264',
      :body => "title of the article
                ====================
                #{@article.title}
                ====================
                and body of the article
                =====================
                #{@article.body}"
    )
  end
end