class ArticlesController < ApplicationController
  require 'rubygems'
  require 'twilio-ruby'


  # for the user simple authentication
  http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]

  def index
    @article=Article.all
  end
  def show
    @article=Article.find(params[:id])
  end
  def new
    @article=Article.new
  end

  def create
    @article= Article.new(article_param)
    if @article.save
      MakeMsgJob.perform_later(@article)
      redirect_to @article
    else
      render :new ,status: :unprocessable_entity
    end
  end

  def edit
    @article=Article.find(params[:id])
  end

  def update
     @article=Article.find(params[:id])
     if @article.update(article_param)
      redirect_to @article #show will class /article/:id
    else
      render :edit,status: :unprocessable_entity
    end
  end

 def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to root_path, status: :see_other
  end



  private 
  def article_param
    params.require(:article).permit(:title,:body,:status)
  end
end
