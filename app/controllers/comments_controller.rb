class CommentsController < ApplicationController

  http_basic_authenticate_with name: "dhh", password: "secret", only: :destroy
	def error
	end
	def show
		@comment=Comment.find(params[:id])
	end
	def create
		@article =Article.find(params[:article_id])
		@comment =@article.comments.new(comment_params)
		if @comment.save
			RegistrationJob.perform_later (@comment)
			redirect_to article_path(@article)
		else
			redirect_to article_path(@article)
		end
	end

	def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article), status: 303
  end


	private
		def comment_params
			params.require(:comment).permit(:commenter,:body,:status,:email)
		end
end
