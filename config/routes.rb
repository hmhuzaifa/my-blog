Rails.application.routes.draw do

  require "sidekiq/web"
  mount Sidekiq::Web => "/sidekiq"

  root "articles#index"
    resources :articles do
      resources :comments
    end
  # resources:articles
  # get "/articles", to: "articles#index"
  # # get "/price",to:"price#show"
  # get "/articles/:id",to: "articles#show"

end
